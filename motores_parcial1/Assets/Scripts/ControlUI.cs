using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlUI : MonoBehaviour
{
    public GameObject imgCompases;
    public Image imgPulsos;
    private Color r;
    private Color g;
    private Color y;

    public TMPro.TMP_Text da�o;
    public GameObject pivoteCentro;
    private Color colorNormal;

   public Image[] imagenes;

    public TMPro.TMP_Text txtVida;

    public TMPro.TMP_Text txtBalas;
    public GameObject cartuchoEscopeta;

    public TMPro.TMP_Text txtCombo;

    public TMPro.TMP_Text txtGameOver;
    public TMPro.TMP_Text txtGameWin;
    // Start is called before the first frame update
    void Start()
    {
        r = new Color(1,0,0,1);
      
      
        g=new Color(0,1,0,1);

        y = new Color(1, 1, 0, 1);

        da�o.enabled = false;
       colorNormal = da�o.color;

        txtBalas.enabled = false;
        cartuchoEscopeta.SetActive(false);

        txtGameOver.enabled = false;
        txtGameWin.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager1.Compases() == true)
        {
            foreach (Image image in imagenes)
            {
                image.color = g;
            }
            
            return;
        }
        else if (GameManager1.Pulsos() == true )
        {


            foreach (Image image in imagenes)
            {
                image.color = y;
            }
            return;
        }
        
        else if(!GameManager1.Pulsos())
        {

            foreach (Image image in imagenes)
            {
                image.color = r;
            }
        }

        desvanecerDa�o();


        if (GameManager1.gameOver)
        {
            txtGameOver.enabled = true;
            //Time.timeScale = 0;
        }
        if (GameManager1.gameWin)
        {
            txtGameWin.enabled = true;
            //Time.timeScale = 0;
        }
    }

    public void actualizarCombo(int combo)
    {
        txtCombo.text = "x"+combo.ToString();
    }
    public void mostrarBalasEscopeta(int cantBalasEscopeta)
    {
        txtBalas.enabled = true;
        txtBalas.text = cantBalasEscopeta.ToString();
        cartuchoEscopeta.SetActive(true);
    }
    public void ocultarBalasEscopeta()
    {
        txtBalas.enabled = false;
        cartuchoEscopeta.SetActive(false);
    }

    public void graficarDa�o(int cantDa�o)
    {
        da�o.text = cantDa�o.ToString();
        da�o.transform.position=pivoteCentro.transform.position;
       int posX= Random.Range(100,180);
        int posY= Random.Range(-180, 180);
        int escala=1;
        da�o.color = colorNormal;
        

        if (cantDa�o > 10000)
        {
            escala = 3;
        }
        else if (cantDa�o > 4000)
        {
            escala = 2;
        }
        else
        {
            escala = 1;
        }
        da�o.enabled = true;

       
        da�o.transform.position=new Vector2 (da�o.transform.position.x+posX,da�o.transform.position.y+posY);
        da�o.transform.localScale=new Vector3( escala,escala);
      
    }


    private void desvanecerDa�o()
    {
       Color desvanecer= new Color(0, 0, 0, 0.5f * Time.deltaTime);
        da�o.color -= desvanecer;
    }

    public void graficarVida(int vida)
    {
        this.txtVida.text = vida.ToString();
    }
}
