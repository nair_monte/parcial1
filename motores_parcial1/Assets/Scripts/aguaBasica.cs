using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class aguaBasica : MonoBehaviour
{

    public GameObject leviatanPrefab;
    public int LeviatanesHaInstanciar;
    private int contLeviatanes;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Player")&&  (contLeviatanes<LeviatanesHaInstanciar) )
        {
            GameObject leviatan = Instantiate(leviatanPrefab, new Vector3(transform.position.x, transform.position.y + 0.5f, transform.position.z), Quaternion.identity);
            contLeviatanes++;
        }
        
    }
}
