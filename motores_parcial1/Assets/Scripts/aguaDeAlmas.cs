using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class aguaDeAlmas : MonoBehaviour
{
    private GameObject player;

    public GameObject almaPrefab;

    private float contCD;
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        contCD += Time.deltaTime;
        float distancia = Vector3.Distance(transform.position, player.transform.position);
        if (distancia <= 25 && contCD>=7f)
        {
            GameObject alma = Instantiate(almaPrefab, transform.position + Vector3.up*2, Quaternion.identity);
            contCD = 0;
        }
    }
}
