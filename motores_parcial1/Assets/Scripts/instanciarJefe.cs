using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class instanciarJefe : MonoBehaviour
{
    private GameObject player;
    public GameObject jefePrefab;
    public GameObject leviatanPrefab;
    int contJefes = 0;
    private float CDSpawnLeviatanes;
    private GameObject jefe;
   
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");  
    }

    // Update is called once per frame
    void Update()
    {
        
        if (CDSpawnLeviatanes<11)
        {
            CDSpawnLeviatanes += Time.deltaTime;
        }
        float distancia = Vector3.Distance(transform.position, player.transform.position);
        
        if (distancia < 20)
        {
          jefe=GameObject.FindGameObjectWithTag("Jefe");
            if (contJefes < 1)
            {
                 jefe = Instantiate(jefePrefab, transform.position, Quaternion.identity);

                contJefes++;
            }
            if (contJefes >= 1 && jefe!=null)
            {
                if (CDSpawnLeviatanes == 10)
                {
                    instanciarLeviatanes(jefe);
                }
                
            }

           
        }

       


       
    }
   private void instanciarLeviatanes(GameObject jefe)
    {
        GameObject leviatan = Instantiate(leviatanPrefab, jefe.transform.position, Quaternion.identity);
    }
}
