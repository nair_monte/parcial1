using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlJugador : MonoBehaviour
{
    public float speed = 5f;
    public float fuerzaSalto = 5f;
    private Rigidbody rb;
    private Vector3 movement;
    public bool enElPiso=true;

    public int contAciertos=0;

    private Animator animator;
    private AudioSource audioSource;
    public AudioClip ataqueEspada;
    public AudioClip recargaEscopeta;
    public AudioClip disparoEscopeta;
    ControlUI controlUI;
    public int vida = 1000;

    private bool espadaActiva;
    private bool escopetaActiva;
    private GameObject espada;
    GameObject escopeta;
    private bool escopetaDesbloqueada = false;

    private float contCDEspada=0;
   private float contCDEscopeta=0;
    private float contCDDash = 0;
    public int cantBalasEscopeta=10;
    private void Awake()
    {
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
        controlUI = GameObject.Find("ControlUI").GetComponent<ControlUI>();
        rb = GetComponent<Rigidbody>();

        espadaActiva = true;

      espada=  GameObject.FindGameObjectWithTag("Espada");
      escopeta= GameObject.FindGameObjectWithTag("Escopeta");

        Cursor.lockState = CursorLockMode.Locked;
    }

    private void Update()
    {
        if (vida <= 0)
        {
            GameManager1.gameOver = true;
        }
       
        if (contCDEscopeta < 10 || contCDEspada < 10)
        {
            contCDEscopeta += Time.deltaTime;
            contCDEspada += Time.deltaTime;
        }

        if (contCDDash < 10)
        {
            contCDDash += Time.deltaTime;
        }
        //animacion recarga escopeta
        
        if (contCDEscopeta < 0.8f && escopetaActiva)
        {
            animator.Play("PlayerRecargandoEscopeta");
            animator.SetBool("recargando", true);

           
        }
        else
        {
            animator.SetBool("recargando", false);
        }

        //salto

        if (Input.GetKeyDown(KeyCode.Space) && enElPiso)
        {
            Vector3 salto = new Vector3(0, fuerzaSalto, 0);
            rb.AddForce(salto, ForceMode.Impulse);

            enElPiso = false;
        }

        //dash
        if (Input.GetKeyDown(KeyCode.LeftShift) && contCDDash>5)
        {
            if(GameManager1.Compases())
            {
                rb.AddForce(transform.forward * 10, ForceMode.VelocityChange);
                contCDDash = 0;
            }

            if (!GameManager1.Compases())
            {
                rb.AddForce(transform.forward * 5, ForceMode.VelocityChange);
                contCDDash = 0;
            }
           
        }

        //armas
        if (espadaActiva)
            espada.SetActive(true);
       
        

        if (escopetaActiva)
            escopeta.SetActive(true);
          

        if (!escopetaActiva)
            escopeta.SetActive(false);

        if (!espadaActiva)
            espada.SetActive(false);

        if (Input.GetMouseButtonDown(0))
        {
            if(espadaActiva&&contCDEspada>= 0.82f)
            {
                contCDEspada = 0;

                if (GameManager1.Compases())
                {
                    audioSource.pitch = 1.5f;
                    audioSource.clip = ataqueEspada;
                    audioSource.Play();
                    animator.Play("ataqueEspadaMejorado");

                    Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));

                    RaycastHit hit;
                    if (Physics.Raycast(ray, out hit, 3f))
                    {
                        if (hit.collider.gameObject.GetComponentInChildren<EnemigoBase>())
                        {

                            EnemigoBase enemigo = hit.collider.gameObject.GetComponentInChildren<EnemigoBase>();


                            enemigo.sacarVida(Critico(1000, 5000));


                            contAciertos++;
                            controlUI.actualizarCombo(contAciertos);
                        }


                    }


                 
                }

                if (!GameManager1.Compases())
                {
                    audioSource.pitch = 1;

                    audioSource.clip = ataqueEspada;
                    audioSource.Play();

                    animator.Play("ataqueEspada");

                    Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));

                    RaycastHit hit;
                    if (Physics.Raycast(ray, out hit, 3f))
                    {
                        if (hit.collider.gameObject.GetComponent<EnemigoBase>())
                        {
                            EnemigoBase enemigo = hit.collider.gameObject.GetComponent<EnemigoBase>();

                            enemigo.sacarVida(500);


                            contAciertos--;
                            controlUI.actualizarCombo(contAciertos);
                        }

                    }



                }

               
            }

            if (escopetaActiva&&contCDEscopeta>= 0.82f && cantBalasEscopeta>0)
            {
                contCDEscopeta = 0;
                cantBalasEscopeta--;
                controlUI.mostrarBalasEscopeta(cantBalasEscopeta);


                audioSource.clip = disparoEscopeta;
                audioSource.PlayOneShot(audioSource.clip);
                 

                if (GameManager1.Compases())
                {
                    
                    Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));

                    RaycastHit hit;
                    if (Physics.Raycast(ray, out hit, 15f))
                    {
                        if (hit.collider.gameObject.GetComponentInChildren<EnemigoBase>())
                        {
                            contAciertos++;
                            controlUI.actualizarCombo(contAciertos);

                            EnemigoBase enemigo = hit.collider.gameObject.GetComponentInChildren<EnemigoBase>();

                            float distancia = Vector3.Distance(transform.position, enemigo.transform.position);

                            if (distancia <= 5)
                            {
                                enemigo.sacarVida(Critico(5000, 7000));

                               
                                return;
                            }
                            else if (distancia <= 10)
                            {
                                enemigo.sacarVida(Critico(3000, 5000));
                                
                              

                                return;
                            }
                            else
                                enemigo.sacarVida(Critico(1000, 2000));
                        
                        }



                    }


                 
                }

                if (!GameManager1.Compases())
                {
              
                    Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));

                    RaycastHit hit;
                    if (Physics.Raycast(ray, out hit, 15f))
                    {
                        if (hit.collider.gameObject.GetComponent<EnemigoBase>())
                        {
                            contAciertos--;
                            controlUI.actualizarCombo(contAciertos);

                            EnemigoBase enemigo = hit.collider.gameObject.GetComponent<EnemigoBase>();

                            float distancia = Vector3.Distance(transform.position, enemigo.transform.position);

                            if (distancia <= 5)
                            {
                                enemigo.sacarVida(1000);
                                
                                return;
                            }
                            else if (distancia <= 10)
                            {
                                enemigo.sacarVida(700);
                               
                                return;
                            }
                            else
                            {
                                enemigo.sacarVida(500);
                               
                            }
                        }

                    }


                   
                }

                
            }
        }

        if (cantBalasEscopeta <= 0)
        {
            cantBalasEscopeta = 0;
        }

        //cambio de armas

        if (Input.GetKey(KeyCode.Alpha1))
        {
            espadaActiva = true;
            escopetaActiva = false;

            controlUI.ocultarBalasEscopeta();
        }
        if (Input.GetKey(KeyCode.Alpha2) && escopetaDesbloqueada)
        {
            escopetaActiva = true;
            espadaActiva = false;

            controlUI.mostrarBalasEscopeta(cantBalasEscopeta);
        }


        //contador de aciertos al ritmo
        if (contAciertos <= 0)
        {
            contAciertos = 0;
            controlUI.actualizarCombo(contAciertos);
        }
        if (contAciertos >= 16)
        {
            contAciertos = 16;
            controlUI.actualizarCombo(contAciertos);
        }
        //else if((Input.GetMouseButtonDown(0) && !GameManager.Tempo()))
            
        //{
            
        //}
    
   
  
    }

    private int Critico(int min, int max)
    {
        int da�o = Random.Range(min, max);

        if (contAciertos >= 0 && contAciertos <= 2)
        {
            da�o = da�o * 1;
        }
        else if (contAciertos >= 3 && contAciertos <= 5)
        {
            da�o = Mathf.RoundToInt(da�o * 1.2f);
        }
        else if (contAciertos >= 6 && contAciertos <= 9)
        {
            da�o = Mathf.RoundToInt(da�o * 1.5f);
        }
        else if (contAciertos >= 10)
        {
            da�o = da�o * 2;
        }
        return da�o;
    }

    public void perderVida(int da�o)
    {
        vida -= da�o;
        controlUI.graficarVida(vida);
        
    }

    public void ganarVida(int vidaExtra)
    {
        vida += vidaExtra;
        if (vida > 1000)
        {
            vida = 1000;
        }
        controlUI.graficarVida(vida);

    }
    private void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        //moveDirection considera hacia donde esta mirando el player, por si el movimiento se va a hacer con rb.velocity
        Vector3 moveDirection = transform.forward * moveVertical + transform.right * moveHorizontal;
       
        //0 y 1
        moveDirection.Normalize();

        movement= moveDirection* speed;

        //movement = new Vector3(moveHorizontal, 0f, moveVertical).normalized * speed;

        //si hay inputs hay movimiento
        if (Input.GetAxis("Horizontal") != 0f || Input.GetAxis("Vertical") != 0f)
        {
         

            rb.MovePosition(rb.position + moveDirection * speed * Time.deltaTime);

            if (escopetaActiva)
            {
                animator.SetBool("enMovimiento", false);
                animator.SetBool("conEscopeta", true);
            }
            else
            {
                animator.SetBool("conEscopeta", false);
                animator.SetBool("enMovimiento", true);
            }
        }
        else
        {
            animator.SetBool("enMovimiento", false);
        }


        ////mov por velocidad
        //rb.velocity = new Vector3(movement.x, rb.velocity.y, movement.z)+Vector3.forward;


       
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Piso"))
        {
            enElPiso = true;
        }

       
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Bala"))
        {
            perderVida(10);
        }
        
        if (other.gameObject.CompareTag("VidaExtra"))
        {
            if (vida <= 999)
            {
                ganarVida(10);
                Destroy(other.gameObject);
            }
           
        }

        if (other.gameObject.CompareTag("EscopetaObjeto"))
        {
            escopetaDesbloqueada = true;
            Destroy(other.gameObject);
        }

        if (other.gameObject.CompareTag("BalasExtra"))
        {
           if(cantBalasEscopeta<30)
            {
                cantBalasEscopeta++;
                controlUI.mostrarBalasEscopeta(cantBalasEscopeta);
                Destroy(other.gameObject);
            }
               
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Agua"))
        {
            perderVida(1);
        }
    }

    private void OnCollisionStay(Collision collision)
    {
       //por cada punto de contacto se pregunta si el otro objeto tiene rb y si es asi se calcula un rebote
        foreach (ContactPoint contact in collision.contacts)
        {
            if (contact.otherCollider.GetComponent<Rigidbody>() != null)
            {
                // se calcula un rebote reflejando el movimiento, la normal apunta hacia afuera
                Vector3 bounceDirection = Vector3.Reflect(movement.normalized, contact.normal);
               

                //reemplaza movement con el vector de rebote
                movement = bounceDirection * speed;
                break;
            }
        }
    }

}
