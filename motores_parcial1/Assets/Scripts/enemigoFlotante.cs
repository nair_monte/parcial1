using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemigoFlotante : MonoBehaviour
{
    private GameObject player;
    public GameObject disparoPrefab;
    public float contCD=0;
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(player.transform);

        contCD += Time.deltaTime;

        float distancia = Vector3.Distance(transform.position, player.transform.position);

        if (contCD >= 5&& distancia<=20f)
        {
            GameObject disparo = Instantiate(disparoPrefab, transform.position, Quaternion.identity);
            
            Vector3 direccion = (player.transform.position - transform.position).normalized;

            disparo.GetComponent<Rigidbody>().velocity = direccion * 50;
           
            contCD = 0;
        }
    
    }
    private void OnTriggerStay(Collider other)
    {
      
       

    }

    private void OnTriggerExit(Collider other)
    {
     
    }

 
}
