using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EnemigoBase : MonoBehaviour
{
    public int vida;
    protected ControlUI controlUI;
    public bool seguirAlPlayer=false;
 
   public GameObject player;
    void Start()
    {
        player = GameObject.Find("jugador");
      
       
       
    }

    // Update is called once per frame
    void Update()
    {
     
    }
    
    public void sacarVida(int da�o)
    {
        controlUI = GameObject.Find("ControlUI").GetComponent<ControlUI>();
       
        controlUI.graficarDa�o(da�o);
        vida -= da�o;

        if (vida <= 0)
        {
            Destroy(this.gameObject);
        }
    }

    public void destruirEnemigo()
    {
        Destroy(this.gameObject);
    }

  
}
