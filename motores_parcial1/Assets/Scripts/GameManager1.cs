using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text.RegularExpressions;
using UnityEngine.SceneManagement;

public class GameManager1 : MonoBehaviour
{
    static List<float> tiemposPulsaciones = new List<float>();
    static List<float> tiemposPulsaciones4 = new List<float>();
   
   static bool cancionIniciada = false;
   public static float rangoTolerancia = 0.1f;

   static float tiempoInicioCancion;

  static  bool tiempoCoincide=false;
  

   
   public static float timer=0;

   public  AudioSource bajo;
    public AudioSource bateria;
    public  AudioSource otros;
    public  AudioSource voz;

    private GameObject player;
    public static bool gameOver=false;
    public static bool gameWin=false;

    private void Awake()
    {
        
     
    }

    void Start()
    {

        IniciarCancion();

        CargarTiemposPulsaciones();
        //
        player = GameObject.Find("jugador");


        //CargarTiemposPulsaciones();
        //IniciarCancion();
    }

    void Update()
    {
        if (cancionIniciada)
        {
            timer += Time.deltaTime;

        }






        //Compases();


        if (Input.GetKeyDown(KeyCode.R))
        {
            Time.timeScale = 1;
            GameManager1.gameWin = false;
            GameManager1.gameOver = false;
            SceneManager.LoadScene("Nivel1");
          
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Time.timeScale = 1;
            GameManager1.gameWin = false;
            GameManager1.gameOver = false;
            SceneManager.LoadScene("Menu");
           
        }



        



        if (player.GetComponent<ControlJugador>().vida <= 0)
        {
            gameOver = true;
        }
        
       else  if (player.GetComponent<ControlJugador>().contAciertos>=0 &&
            player.GetComponent<ControlJugador>().contAciertos < 3)
        {
            bateria.volume = 1f;
            bajo.volume = 0f;
            otros.volume = 0f;
            voz.volume = 0f;
            return;
        }
       else if (player.GetComponent<ControlJugador>().contAciertos >= 3 &&
            player.GetComponent<ControlJugador>().contAciertos < 6)
            
        {
            bateria.volume = 1f;
            bajo.volume = 1f;
            otros.volume = 0f;
            voz.volume = 0f;
            return;
        }
       else if (player.GetComponent<ControlJugador>().contAciertos >= 6 &&
            player.GetComponent<ControlJugador>().contAciertos < 10)
        {
            bateria.volume = 1f;
            bajo.volume = 1f;
            otros.volume = 1f;
            voz.volume = 0f;
            return;
        }
       else if (player.GetComponent<ControlJugador>().contAciertos >= 10 &&
            player.GetComponent<ControlJugador>().contAciertos <= 16)
        {
            bateria.volume = 1f;
            bajo.volume = 1f;
            otros.volume = 1f;
            voz.volume = 1f;
            return;
        }



        //grabar tiempos de pulsaciones en la consola
        //if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.RightArrow))
        //{
        //    float tiempoPulsacion = timer;
        //    Debug.Log("Tiempo de pulsación: " + tiempoPulsacion);
        //    tiemposPulsaciones.Add(tiempoPulsacion);
        //}

    }


   static bool VerificarCoincidencia()
    {

        //recorre la lista, asignando cada valor a tiempoAlmacenado y comparando el tiempoAlmacenado con el tiemp que le pasamos (tiempoPulsacion)
        foreach (float tiempoAlmacenado in tiemposPulsaciones)
        {

            //si el tiempoPulsacion - tiempoAlmacenado esta entre -rangoTolerancia y +rangoTolerancia hay coincidencia
            // tiempoActual - tiempoAlmacenado==0 seria coincidencia perfecta pero se considera el margen de error positivo y negativo
            if ((timer + rangoTolerancia >= tiempoAlmacenado  &&
                 timer - rangoTolerancia <= tiempoAlmacenado))
            {
                return true;
            }
           
        }

        return false;
    }

    static bool VerificarCoincidenciaPulsos4()
    {

        //recorre la lista, asignando cada valor a tiempoAlmacenado y comparando el tiempoAlmacenado con el tiemp que le pasamos (tiempoPulsacion)
        foreach (float tiempoAlmacenado in tiemposPulsaciones4)
        {

            //si el tiempoPulsacion - tiempoAlmacenado esta entre -rangoTolerancia y +rangoTolerancia hay coincidencia
            // tiempoActual - tiempoAlmacenado==0 seria coincidencia perfecta pero se considera el margen de error positivo y negativo
            if ((timer + rangoTolerancia >= tiempoAlmacenado &&
                 timer - rangoTolerancia <= tiempoAlmacenado))
            {
                return true;
            }

        }

        return false;
    }

    void IniciarCancion()
    {
        bateria.Play();
        bajo.Play();
        otros.Play();
        voz.Play();

        bateria.volume = 1f;
        bajo.volume = 0f;
        otros.volume = 0f;
        voz.volume = 0f;
       

        tiempoInicioCancion = Time.time;
       
        cancionIniciada = true;
        
        //inicia el timer de la cancion
      timer = Time.time - tiempoInicioCancion;
    }


    void CargarTiemposPulsaciones()
    {
        string rutaArchivo = "Assets/Resources/bpmPorCompas.txt";
        //lee el txt y guarda cada linea de texto en un array de strings    
        TextAsset txtAsset = Resources.Load<TextAsset>("bpmPorCompas");
        string[] lineas = txtAsset.text.Split('\n');

        tiemposPulsaciones = new List<float>();
        tiemposPulsaciones4 = new List<float>();

        float ultimoPulso = 0;
        float siguientePulso = 0;
        float siguienteCompas = 0;
        
        
        //por compas
        //recorre el array de strings lineas y por cada linea pregunta si se puede convertir el string en un float
        foreach (string linea in lineas)
        {
            
            //.Trim elimina los espacios en blanco
            if (float.TryParse(linea.Trim(), out float bpm))
            {
                
                //pasar los bpm a segundos
                siguientePulso = (60 / bpm)*1000;

                siguienteCompas = siguientePulso * 4;

               

                ultimoPulso += siguienteCompas;
                //si se pudo convertir se agrega el float a una lista de pulsaciones 
                tiemposPulsaciones.Add(ultimoPulso);
                

              
            }
        }

        ultimoPulso = 0;
        //por pulsos
        foreach (string linea in lineas)
        {

            //.Trim elimina los espacios en blanco
            if (float.TryParse(linea.Trim(), out float bpm))
            {

                //pasar los bpm a segundos
                siguientePulso = (60 / bpm) * 1000;

                

                //al pasar el bpm de un compas hay que agregar 4 pulsos 

                ultimoPulso += siguientePulso;
                //si se pudo convertir se agrega el float a una lista de pulsaciones 
                tiemposPulsaciones4.Add(ultimoPulso);

                ultimoPulso += siguientePulso;               
                tiemposPulsaciones4.Add(ultimoPulso);

                ultimoPulso += siguientePulso;
                tiemposPulsaciones4.Add(ultimoPulso);
                
                ultimoPulso += siguientePulso;
                tiemposPulsaciones4.Add(ultimoPulso);


            }
        }
    }

    public static bool Compases()
    {
        bool compas = false;

        if (cancionIniciada)
        {
            
         
            tiempoCoincide = VerificarCoincidencia();

            if (tiempoCoincide)
            {

                compas = true;
                return compas;
            }
            else
            {

                compas = false;

                return compas;
                //intervalo de tiempo para la siguiente pulsacion valida
                //tiempoSiguientePulsacion = timer + rangoTolerancia;
            }


            //if (timer >= tiempoSiguientePulsacion)

            //{
                

            //}

            
        }
        return compas;
    }

    public static bool Pulsos()
    {
        bool pulso = false;

        if (cancionIniciada)
        {


            tiempoCoincide = VerificarCoincidenciaPulsos4();

            if (tiempoCoincide)
            {

                pulso = true;
                return pulso;
            }
            else
            {

                pulso = false;

                return pulso;
           
            }

        }
        return pulso;
    }
}
