using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Leviatan : EnemigoBase
{

    private NavMeshAgent navMeshAgent;
    

    private Animator animator;

    public bool esUnJefe=false;
  
    void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        player = GameObject.Find("jugador");
        
        animator = GetComponent<Animator>();

       
    }

    // Update is called once per frame
    void Update()
    {
        navMeshAgent.SetDestination(player.transform.position);

      
    }

    private void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.CompareTag("Player"))
        {
            if (esUnJefe)
            {
                collision.gameObject.GetComponent<ControlJugador>().perderVida(50);
            }
            else
            {
                collision.gameObject.GetComponent<ControlJugador>().perderVida(10);
            }

        }
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            animator.SetBool("atacando", true);
            Invoke("resetAtaque", 0.32f);
           
        }
      
    }

    private void resetAtaque()
    {
        animator.SetBool("atacando", false);
    }
   
}
